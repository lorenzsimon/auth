FROM maven:3.6.3-jdk-11
COPY ./service .
RUN mvn install -Dmaven.test.skip=true

FROM openjdk:11
COPY --from=0 /target/*.jar .
CMD java -jar *.jar
