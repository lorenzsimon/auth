# Documentation of the Auth-Service:

## Main purpose of the Auth-Service?

The Auth-Service handles the authentication of users. Some parts of Boogle are exclusive to logged on users, or even only accessible for admins.
In order to check for these access rights, a jwt (also referred to as access token) is created, which contains all necessary data for user authentication and authorization (the latter is the Gateway's job).
##### The email of the admin is "admin", the password is "password".

## Internal Service Logic
The Auth Service consists of two components, the MariaDB database and the Java Spring Application.

More information about the Database is in the [Database Wiki](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/auth/-/wikis/Auth-Database).

A full list of available endpoints is in the Auth-Service [Wiki](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/auth/-/wikis/Auth-API).

The used libraries are listed [here](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/auth/-/wikis/Libraries).

The payloads of the tokens look like [this](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/auth/-/wikis/Token-Payloads).

More information about refresh tokens can be found [here](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/auth/-/wikis/Refresh-Tokens).

The different roles are listed & explained [here](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/auth/-/wikis/Roles).

Last but no least, the [Github AOuth2 Flow](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/auth/-/wikis/OAuth2-Github-Flow).

## Known Bugs
It is NOT recommended to use the Github Login. If the user is already logged in over at Github, the request might return an Internal Server Error. Trying again solves the problem most of the time though. Once the user is logged in with his Github account, he can call mappings for authenticated users, but some other features might not be working correctly or even missing, such as changing the username or deleting the user. These were primarily developed for users who use our own login.

Also, the admin can delete himself, which is probably more of a feature than a bug. Since the admin has the highest order of control about the application, why shouldn't he be able to? In case of an empty credentials table, the admin is created again. Other than that, there is no way to create a new admin at the moment. 
