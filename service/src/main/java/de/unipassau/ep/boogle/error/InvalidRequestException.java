package de.unipassau.ep.boogle.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This runtime exception indicates that the format of the request is not readable by the backend.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidRequestException extends BoogleAuthException {
    public InvalidRequestException() {
        super("Invalid Request.");
    }
}
