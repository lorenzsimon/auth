package de.unipassau.ep.boogle.repository;

import de.unipassau.ep.boogle.model.entity.Credentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This repository connects the service to the authentication database (db-auth).
 * This class in particular maps to the credentials table.
 */
@Repository
public interface CredentialsRepository extends JpaRepository<Credentials, Integer> {

    /**
     * Looks up a {@link Credentials} object by its email in the DB.
     *
     * @param email The email of the requested userCredentials.
     *
     * @return Returns the {@link Credentials} if such an email exists in the DB, {@code null} else.
     */
    Credentials findByEmail(String email);
}
