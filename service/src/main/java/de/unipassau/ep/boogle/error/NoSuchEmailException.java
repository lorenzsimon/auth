package de.unipassau.ep.boogle.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This runtime exception tells the user, who tries to log in, that the the email provided by him is not registered.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NoSuchEmailException extends BoogleAuthException {
    public NoSuchEmailException() {
        super("The provided email does not belong to any account.");
    }
}