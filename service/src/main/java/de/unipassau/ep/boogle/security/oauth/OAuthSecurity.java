package de.unipassau.ep.boogle.security.oauth;

import de.unipassau.ep.boogle.service.CredentialsService;
import de.unipassau.ep.boogle.service.OAuthCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * The security config class tailored to the needs of our JWT authentication implementation.
 */
@Configuration
@EnableWebSecurity
@Order(Ordered.HIGHEST_PRECEDENCE)
class OAuthSecurity extends WebSecurityConfigurerAdapter {

    private final OAuthCredentialsService oAuthCredentialsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Injects all dependencies.
     *
     * @param oAuthCredentialsService The OAuthCredentialsService.
     * @param bCryptPasswordEncoder The PasswordEncoder.
     */
    @Autowired
    public OAuthSecurity(OAuthCredentialsService oAuthCredentialsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.oAuthCredentialsService = oAuthCredentialsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Configures the security of the Boogle endpoints and adds the Authorization and Authentication filters.
     *
     * @param http The {@link HttpSecurity} object that is getting configured.
     * @throws Exception Throws an Exception in case of an error.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requestMatchers()
                .antMatchers("/login2")
                .and().cors().and().csrf().disable().authorizeRequests()
                .antMatchers(
                        HttpMethod.POST,
                        "/login2"
                ).permitAll() // not yet using zuul & eureka
                .anyRequest().authenticated()
                .and()
                .addFilter(new OAuthAuthenticationFilter(authenticationManager()));
                //.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    /**
     * Configures the {@link AuthenticationManager} to use the {@link CredentialsService} as its
     * {@link org.springframework.security.core.userdetails.UserDetailsService} and the {@link BCryptPasswordEncoder} as
     * its {@link org.springframework.security.crypto.password.PasswordEncoder}.
     *
     * @param auth The {@link AuthenticationManagerBuilder} used to configure the {@link AuthenticationManager}.
     * @throws Exception Throws an Exception in case of an error.
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(oAuthCredentialsService).passwordEncoder(bCryptPasswordEncoder);
    }
}
