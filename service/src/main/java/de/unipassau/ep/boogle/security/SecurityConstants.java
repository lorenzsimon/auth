package de.unipassau.ep.boogle.security;

/**
 * This class contains important constants that should not be changed (except maybe TTL for security optimizations).
 */
public class SecurityConstants {

    public static final int TTL = 1000 * 60 * 5; // time to live: 5 minutes
    public static final int REFRESH_TTL = 1000 * 60 * 60 * 24; // ttl for refresh tokens: 1 day
    public static final String SECRET_KEY = "please1don't2tell3anyone4about5this!";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTH_HEADER = "Authorization";
    public static final String SIGN_UP_URL = "/signup";
    public static final String LOGIN_URL = "/login";
}
