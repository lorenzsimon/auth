package de.unipassau.ep.boogle.error;

/**
 * The parent class of all custom auth runtime exceptions. When a method throws this exception, it has to be one of its
 * subclasses that is actually thrown in the body of the method.
 * These are especially helpful for web development, because if on of these exceptions is ever thrown, it short circuits
 * the rest of the remaining code and sends the respective status code directly to the client.
 */
public abstract class BoogleAuthException extends RuntimeException {
    public BoogleAuthException(String message) {
        super(message);
    }
}
