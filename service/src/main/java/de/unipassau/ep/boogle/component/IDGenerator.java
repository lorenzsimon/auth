package de.unipassau.ep.boogle.component;

import de.unipassau.ep.boogle.service.CredentialsService;
import de.unipassau.ep.boogle.service.OAuthCredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * This component class generates a unique id, considering the id columns of both the credentials and oauth_credentials
 * tables. So it is kind of like a unique constraint but considering two different classes.
 */
@Component
public class IDGenerator {
    private final CredentialsService credentialsService;
    private final OAuthCredentialsService oAuthCredentialsService;
    private static int nextID;

    /**
     * Injects the necessary dependencies.
     *
     * @param credentialsService The service that has to return its currently highest id.
     * @param oAuthCredentialsService The service that has to return its currently highest id.
     */
    @Autowired
    public IDGenerator(CredentialsService credentialsService, OAuthCredentialsService oAuthCredentialsService) {
        this.credentialsService = credentialsService;
        this.oAuthCredentialsService = oAuthCredentialsService;
    }

    /**
     * Returns the next ID that can be used without violation the unique constraint.
     *
     * @return Returns the next id.
     */
    public static int getNextID() {
        int thisID = nextID;
        nextID++;
        return thisID;
    }

    /**
     * After startup, the nextId is set to next id that has not been taken yet.
     */
    @PostConstruct
    private void determineNextID() {
        int max1 = credentialsService.getCurrentMaxID();
        int max2 = oAuthCredentialsService.getCurrentMaxID();
        nextID = Math.max(max1, max2) + 1;
    }
}
