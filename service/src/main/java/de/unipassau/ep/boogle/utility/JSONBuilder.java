package de.unipassau.ep.boogle.utility;

import org.springframework.stereotype.Component;

import java.util.*;

/**
 * A builder class that makes building JSON with auth related keys a lot easier.
 */
@Component
public class JSONBuilder {

    private static final String INDENTATION = "    ";
    private final Map<String, String> map;

    /**
     * Constructs a new JSONBuilder with predefined keys, but no values at this point.
     */
    public JSONBuilder() {
        map = new HashMap<>();
        map.put("id", null);
        map.put("email", null);
        map.put("token", null);
    }

    /**
     * Needs to be the first method of the chain, because it resets the values of the maps.
     *
     * @return Returns the builder with an empty, completely modifiable map.
     */
    public JSONBuilder build() {
        map.replaceAll((k, v) -> null);
        return this;
    }

    /**
     * Sets the id to the given String.
     *
     * @param id The new id.
     * @return Returns the modified JSONBuilder for further modification.
     */
    public JSONBuilder setID(String id) {
        map.put("id", id);
        return this;
    }

    /**
     * Sets the email to the given String.
     *
     * @param email The new email.
     * @return Returns the modified JSONBuilder for further modification.
     */
    public JSONBuilder setEmail(String email) {
        map.put("email", email);
        return this;
    }

    /**
     * Sets the token to the given String.
     *
     * @param token The new token.
     * @return Returns the modified JSONBuilder for further modification.
     */
    public JSONBuilder setToken(String token) {
        map.put("token", token);
        return this;
    }

    /**
     * Allows for additional key value pairs.
     *
     * @param key The new key getting added to the json.
     * @param value The new value getting added to the JSON.
     * @return Returns the modified JSONBuilder for further modification.
     */
    public JSONBuilder setAdditionalKeyValuePair(String key, String value) {
        map.put(key, value);
        return this;
    }

    /**
     * Adds a key value pair to the json String in a new line.
     *
     * @param key The new key getting added to the String.
     * @param value The new value getting added to the String.
     * @return Returns the whole line, containing both the key and the value.
     */
    private String addLine(String key, String value) {
        return "\n" + INDENTATION + "\"" + key + "\":\"" + value + "\",";
    }

    /**
     * Takes the map, and turns it into a valid JSON. Empty keys (==null) are ignored.
     *
     * @return Returns a JSON built from the hashmap.
     */
    public String toJSON() {
        StringBuilder body = new StringBuilder("{");
        for (String key : map.keySet()) {
            if (map.get(key) != null)
                body.append(addLine(key, map.get(key)));
        }
        body.setLength(body.length() - 1); // removes ',' after last kay value pair
        body.append("\n}");
        return body.toString();
    }
}