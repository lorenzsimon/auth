package de.unipassau.ep.boogle.model.entity;

/**
 * This enum defines the two possible roles a Boogle user can have.
 */
public enum Role {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_USER("ROLE_USER");

    private final String name;

    /**
     * The constructor, setting the name of the role.
     *
     * @param name The String representation of the role.
     */
    Role(String name) {
        this.name = name;
    }

    /**
     * Gets the String representation of the role.
     *
     * @return Returns the name of this role.
     */
    @Override
    public String toString() {
        return name;
    }
}
