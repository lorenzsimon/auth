package de.unipassau.ep.boogle.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.unipassau.ep.boogle.error.InvalidRequestException;
import de.unipassau.ep.boogle.model.entity.Credentials;
import de.unipassau.ep.boogle.model.entity.OAuthCredentials;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * This utility class contains some static methods that handle JSON operations such as building or mapping.
 */
public class JSONUtility {

    /**
     * Gets the value of the provided key from the given json string.
     *
     * @param json The json String that is being parsed.
     * @param key The key of which the value should be returned.
     * @return Returns the value of the given key if the key exists, or null else.
     */
    public static String getValueFromKey(String json, String key) {
        ObjectNode node = null;
        try {
            node = new ObjectMapper().readValue(json, ObjectNode.class);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        if (node != null && node.has(key)) {
            String value = node.get(key).toString();
            char[] valueArray = value.toCharArray();
            if (valueArray[0] == '\"' && valueArray[valueArray.length - 1] == '\"') {
                value = value.substring(1, value.length() - 1);  // removes quotation marks in case of strings
            }
            return value;
        }
        throw new InvalidRequestException();
    }

    /**
     * Maps the json containing the email and password of a user to a {@link Credentials} object.
     *
     * @param json The given json string containing the values.
     * @return Returns the credentials if the needed values were provided by the json, and null if not.
     */
    public static Credentials mapToCredentialObject(BufferedReader json) {
        try {
            return new ObjectMapper().readValue(json, Credentials.class);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Maps the json containing the email and password of a user to a {@link OAuthCredentials} object.
     *
     * @param json The given json string containing the values.
     * @return Returns the oauth_credentials if the needed values were provided by the json, and null if not.
     */
    public static OAuthCredentials mapToOAuthCredentialObject(BufferedReader json) {
        try {
            return new ObjectMapper().readValue(json, OAuthCredentials.class);
        } catch (IOException e) {
            return null;
        }
    }
}
