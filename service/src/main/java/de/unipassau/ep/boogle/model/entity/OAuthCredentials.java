package de.unipassau.ep.boogle.model.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * This entity class models all data necessary to uniquely identify and authenticate a user who logs in with their
 * github account.
 */
@Entity
public class OAuthCredentials implements Serializable {

    @Id
    @Column(unique = true)
    private int id;

    private String githubId;

    private String nodeId;

    private Role role;

    /**
     * The empty constructor for hibernate.
     */
    public OAuthCredentials() {

    }

    /**
     * Gets the id of a user
     *
     * @return Returns the id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of a user. Don't use, because this is already handled by JPA.
     *
     * @param id The new id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the GithubId of a user.
     *
     * @return Returns the GithubId.
     */
    public String getGithubId() {
        return githubId;
    }

    /**
     * Sets the GithubId of a user.
     *
     * @param githubId The new GithubId.
     */
    public void setGithubId(String githubId) {
        this.githubId = githubId;
    }

    /**
     * Gets the NodeId of a user.
     *
     * @return Returns the NodeId. Raw if not already saved in the DB, hashed, else.
     */
    public String getNodeId() {
        return nodeId;
    }

    /**
     * Sets the NodeId of a user.
     *
     * @param nodeId The new NodeId.
     */
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * Sets the role of a user/admin.
     *
     * @param role The role getting set.
     */
    public void setRole(Role role) {
        this.role = role;
    }
}
