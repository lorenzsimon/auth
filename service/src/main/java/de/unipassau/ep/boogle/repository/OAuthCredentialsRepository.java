package de.unipassau.ep.boogle.repository;

import de.unipassau.ep.boogle.model.entity.OAuthCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This repository connects the service to the authentication database (db-auth).
 * This class in particular maps to the oauth_credentials table.
 */
@Repository
public interface OAuthCredentialsRepository extends JpaRepository<OAuthCredentials, Integer> {

    /**
     * Looks up a {@link OAuthCredentials} object by its GithubId in the DB.
     *
     * @param githubId The GithubId of the requested userCredentials.
     *
     * @return Returns the {@link OAuthCredentials} if such a GithubId exists in the DB, {@code null} else.
     */
    OAuthCredentials findByGithubId(String githubId);

}
