package de.unipassau.ep.boogle.security.oauth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import de.unipassau.ep.boogle.model.entity.OAuthCredentials;
import de.unipassau.ep.boogle.service.OAuthCredentialsService;
import de.unipassau.ep.boogle.utility.JSONBuilder;
import de.unipassau.ep.boogle.utility.JSONUtility;
import org.apache.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

import static de.unipassau.ep.boogle.security.SecurityConstants.*;

/**
 * The custom implementation of {@link UsernamePasswordAuthenticationFilter} for OAuth2 credentials.
 * The filter is applied to users who are trying to log in with their Github credentials.
 */
public class OAuthAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private OAuthCredentialsService oAuthCredentialsService;
    private JSONBuilder jsonBuilder;

    /**
     * Injects all dependencies.
     *
     * @param authenticationManager The given {@link AuthenticationManager}.
     */
    public OAuthAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;

        // filter gets triggered when /login2 is accessed via POST
        setFilterProcessesUrl("/login2");
    }

    /**
     * Tries to map the values of the incoming JSON to the {@link OAuthCredentials} java object by comparing the keys to
     * the attribute names. These values are then compared to the stored credentials.
     * This time, the credentials are provided by Github, not the frontend.
     *
     * If everything works and the credentials are valid, an authentication is returned and
     * {@link OAuthAuthenticationFilter#successfulAuthentication(HttpServletRequest, HttpServletResponse, FilterChain,
     * Authentication)} gets called.
     *
     * @param req The incoming request.
     * @param res The response that is built here and sent back to the client.
     * @return Returns an {@link Authentication} if the user could be authenticated, null else.
     * @throws AuthenticationException Thrown if the authentication went wrong.
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException {
        BufferedReader reader;
        try {
            reader = req.getReader();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        initUninitializedComponents(req);

        OAuthCredentials oAuthCredentials = JSONUtility.mapToOAuthCredentialObject(reader);
        if (oAuthCredentials != null) {
            try {
                return authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                oAuthCredentials.getGithubId(),
                                oAuthCredentials.getNodeId(),
                                new ArrayList<>()) // not setting any roles here yet
                );
            } catch (AuthenticationException e) {
                unsuccessfulAuthentication(res);
            }
        } else {
            try {
                res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Request.");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    /**
     * Gets called when the provided oauth_credentials could be verified. The method creates a token based on the given
     * {@link Authentication} object, and writes it to the body of the response (coupled with the id, so the frontend
     * has a reference to the user). It also creates a refresh token with less data in its payload.
     *
     * @param req The incoming request.
     * @param res The response that is build here and sent back to the client.
     * @param chain The chain is unused.
     * @param auth The Authentication object used to get the identity of the authenticated principal. In case of
     *             githubId and nodeId, it is the githubId.
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res,
                                            FilterChain chain, Authentication auth)  {
        PrintWriter writer;
        try {
            writer = res.getWriter();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        OAuthCredentials user = oAuthCredentialsService.getUserByGithubId(((User) auth.getPrincipal()).getUsername());
        int id = user.getId();
        String token = JWT.create()

                // note that the getUsername() method refers to the email in our implementation
                .withSubject(((User) auth.getPrincipal()).getUsername())

                // the claim defines the role of the user, which is later translated into an authority
                .withClaim("isAdmin", false)
                .withClaim("id", id)

                // this ensures that no access token can be used to get new tokens
                .withClaim("isRefresh", false)

                .withExpiresAt(new Date(System.currentTimeMillis() + TTL))
                .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));

        String refreshToken = JWT.create()
                .withClaim("id", id)

                // this ensures that no access token can be used to get new tokens
                .withClaim("isRefresh", true)

                .withClaim("isOAuth", true)

                .withExpiresAt(new Date(System.currentTimeMillis() + REFRESH_TTL))
                .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));


        String body = jsonBuilder
                .build()
                .setID(Integer.toString(id))
                .setToken(token)
                .setAdditionalKeyValuePair("refresh_token", refreshToken)
                .toJSON();

        writer.write(body);
        writer.flush();
    }

    /**
     * Gets called when something went wrong during the authentication process.
     * Since the whole Github OAuth flow is mostly out of our control, we simply return an unknown error.
     *
     * @param res The response containing the error message as information for the user, who is trying to authenticate
     *            themselves.
     */
    private void unsuccessfulAuthentication(HttpServletResponse res) {
        try {
            res.sendError(HttpStatus.SC_INTERNAL_SERVER_ERROR, "An unknown error occurred during Github OAuth2 "
                    + "login. Please try again.");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Lazily initializes the components on the first call, as they cannot be autowired in a filter class without
     * further configuration.
     *
     * @param req The request that defines the servlet context.
     */
    private void initUninitializedComponents(HttpServletRequest req) {
        ServletContext servletContext = req.getServletContext();
        WebApplicationContext webApplicationContext =
                WebApplicationContextUtils.getWebApplicationContext(servletContext);
        assert webApplicationContext != null;
        if (oAuthCredentialsService == null) {
            oAuthCredentialsService = webApplicationContext.getBean(OAuthCredentialsService.class);
        }
        if (jsonBuilder == null) {
            jsonBuilder = webApplicationContext.getBean(JSONBuilder.class);
        }
    }
}