package de.unipassau.ep.boogle.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * This class serves as an entity class for blacklisted refresh tokens.
 */
@Entity
public class BlacklistedToken {

    @Id
    @Column(unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    private String token;

    private Date expires;

    /**
     * Empty constructor for hibernate.
     */
    public BlacklistedToken() {
    }

    /**
     * Gets the id.
     *
     * @return Returns the id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id to the given value.
     * @param id The given id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the token.
     *
     * @return Returns the token.
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the token to the given value.
     * @param token The given token.
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Gets the expiration date.
     *
     * @return Returns the expiration date.
     */
    public Date getExpires() {
        return expires;
    }

    /**
     * Sets the expiration date.
     * @param expires The given expiration date.
     */
    public void setExpires(Date expires) {
        this.expires = expires;
    }
}
