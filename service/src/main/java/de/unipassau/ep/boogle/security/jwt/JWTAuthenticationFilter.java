package de.unipassau.ep.boogle.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import de.unipassau.ep.boogle.error.BoogleAuthException;
import de.unipassau.ep.boogle.model.entity.Credentials;
import de.unipassau.ep.boogle.model.entity.Role;
import de.unipassau.ep.boogle.service.CredentialsService;
import de.unipassau.ep.boogle.utility.JSONBuilder;
import de.unipassau.ep.boogle.utility.JSONUtility;
import org.apache.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

import static de.unipassau.ep.boogle.security.SecurityConstants.*;

/**
 * The custom implementation of {@link UsernamePasswordAuthenticationFilter} for JWT support. The filter is applied to
 * users who are trying to log in with their Boogle credentials.
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private CredentialsService credentialsService;
    private JSONBuilder jsonBuilder;

    /**
     * The constructor setting the {@link AuthenticationManager} to the given one.
     *
     * @param authenticationManager The given {@link AuthenticationManager}.
     */
    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    /**
     * Gets called whenever a user calls /login.
     * The values of the incoming JSON are mapped to a {@link Credentials} object.
     * These values are then compared to the stored credentials.
     *
     * If everything works and the credentials are valid, an authentication is returned and
     * {@link JWTAuthenticationFilter#successfulAuthentication(HttpServletRequest, HttpServletResponse, FilterChain,
     * Authentication)} gets called.
     *
     * If not, the method throws a {@link AuthenticationException} or any of its inheriting exceptions, and calls the
     * {@link JWTAuthenticationFilter#unsuccessfulAuthentication(Credentials, HttpServletResponse)} method.
     *
     * @param req The incoming request.
     * @param res The response that is built here and sent back to the client.
     * @return Returns an {@link Authentication} if the user could be authenticated, null else.
     * @throws AuthenticationException Thrown if the authentication went wrong.
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
                                                                                throws AuthenticationException {
        BufferedReader reader;
        try {
            reader = req.getReader();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        initUninitializedComponents(req);

        Credentials credentials = JSONUtility.mapToCredentialObject(reader);
        if (credentials != null) {
            try {
                return authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                credentials.getEmail(),
                                credentials.getPassword(),
                                new ArrayList<>()) // not setting any roles here yet
                );
            } catch (AuthenticationException e) {
                unsuccessfulAuthentication(credentials, res);
            }
        } else {
            try {
                res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Request.");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    /**
     * Gets called when the provided credentials could be verified. The method creates an access token based on the
     * given {@link Authentication} object, and writes it to the body of the response (coupled with the id, so the
     * client has a reference to the user). It also creates a refresh token with less data in its payload.
     *
     * @param req The incoming request.
     * @param res The response that is build here and sent back to the client.
     * @param chain The chain is unused.
     * @param auth The Authentication object used to get the identity of the authenticated principal. In case of email
     *             and password, it is the email.
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res,
                                            FilterChain chain, Authentication auth) {
        PrintWriter writer;
        try {
            writer = res.getWriter();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // never throws a NoSuchEmailException here as the successfulAuthentication method implies a correct email
        Credentials user = credentialsService.getUserByEmail(((User) auth.getPrincipal()).getUsername());
        boolean isAdmin = user.getRole() == Role.ROLE_ADMIN;
        int id = user.getId();
        String token = JWT.create()

                // note that the getUsername() method refers to the email in our implementation
                .withSubject(((User) auth.getPrincipal()).getUsername())

                // the claim defines the role of the user, which is later translated into an authority
                .withClaim("isAdmin", isAdmin)
                .withClaim("id", id)

                // this ensures that no access token can be used to get new tokens
                .withClaim("isRefresh", false)

                .withExpiresAt(new Date(System.currentTimeMillis() + TTL))
                .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));


        String refreshToken = JWT.create()
                .withClaim("id", id)

                // this ensures that no access token can be used to get new tokens
                .withClaim("isRefresh", true)

                .withClaim("isOAuth", false)

                .withExpiresAt(new Date(System.currentTimeMillis() + REFRESH_TTL))
                .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));


        String body = jsonBuilder
                .build()
                .setID(Integer.toString(id))
                .setToken(token)
                .setAdditionalKeyValuePair("refresh_token", refreshToken)
                .toJSON();

        writer.write(body);
        writer.flush();
    }

    /**
     * Gets called when something went wrong during the authentication process. Does not override the
     * {@link UsernamePasswordAuthenticationFilter#unsuccessfulAuthentication(HttpServletRequest, HttpServletResponse,
     * AuthenticationException)} method, because the credentials object is needed for determining the cause of the
     * error.
     * The method leaves it to the {@link CredentialsService} to find the cause of the Exception, and writes the
     * according error message to the body.
     *
     * @param credentials The incorrect credentials.
     * @param res The response containing the error message as information for the user, who is trying to authenticate
     *            themselves.
     */
    private void unsuccessfulAuthentication(Credentials credentials, HttpServletResponse res) {

        BoogleAuthException exception = credentialsService.determineAuthenticationError(credentials);
        try {
            // can not be done by simply throwing an Exception, since the return type is void.
            // that would always result in a 500 Internal Server Error with unknown cause,
            // which would not be very helpful, especially since this is probably the most important
            // error handling of the entire application
            res.sendError(HttpStatus.SC_BAD_REQUEST, exception.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Lazily initializes the Components on the first call, as they cannot be autowired in a filter class without
     * further configuration.
     *
     * @param req The request that defines the servlet context.
     */
    private void initUninitializedComponents(HttpServletRequest req) {
        ServletContext servletContext = req.getServletContext();
        WebApplicationContext webApplicationContext =
                WebApplicationContextUtils.getWebApplicationContext(servletContext);
        assert webApplicationContext != null;
        if (credentialsService == null) {
            credentialsService = webApplicationContext.getBean(CredentialsService.class);
        }
        if (jsonBuilder == null) {
            jsonBuilder = webApplicationContext.getBean(JSONBuilder.class);
        }
    }
}

