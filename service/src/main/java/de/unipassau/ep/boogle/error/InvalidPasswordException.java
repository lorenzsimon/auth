package de.unipassau.ep.boogle.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This runtime exception tells the user the the password provided by him doesn't belong to the given email.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidPasswordException extends BoogleAuthException {
    public InvalidPasswordException() {
        super("Wrong Password.");
    }
}