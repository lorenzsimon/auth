package de.unipassau.ep.boogle.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This runtime exception tells the user the the email provided by him is already in use by another user.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DuplicateEmailException extends BoogleAuthException {
    public DuplicateEmailException() {
        super("Email is already taken.");
    }
}
