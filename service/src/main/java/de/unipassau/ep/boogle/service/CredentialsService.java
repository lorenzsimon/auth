package de.unipassau.ep.boogle.service;

import de.unipassau.ep.boogle.component.IDGenerator;
import de.unipassau.ep.boogle.error.*;
import de.unipassau.ep.boogle.model.entity.Credentials;
import de.unipassau.ep.boogle.model.entity.Role;
import de.unipassau.ep.boogle.repository.CredentialsRepository;
import de.unipassau.ep.boogle.security.jwt.JWTAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * This service class handles {@link Credentials}. It serves as an interface between the controller and the database.
 */
@Service
public class CredentialsService implements UserDetailsService {

    private final CredentialsRepository credentialsRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * The constructor injecting the {@link CredentialsRepository} and {@link BCryptPasswordEncoder} dependencies.
     *
     * @param credentialsRepository The java connection to the db-auth.
     * @param bCryptPasswordEncoder The encoder used to encrypt passwords.
     */
    @Autowired
    public CredentialsService(CredentialsRepository credentialsRepository,
                              BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.credentialsRepository = credentialsRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Gets a new {@link UserDetails} object of the {@link Credentials} object with the given email. This is needed
     * internally for spring security; that's also why it returns a UserDetails object instead of Credentials.
     *
     * @param email The email of the requested UserDetails object.
     * @return Returns a UserDetails object with the given email if it exists.
     * @throws UsernameNotFoundException Thrown if a user with the given email could not be found.
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Credentials credentials = credentialsRepository.findByEmail(email);
        return new User(credentials.getEmail(), credentials.getPassword(), new ArrayList<>());
    }

    /**
     * Gets the user by the given email.
     * @param email The email that the to be returned user has.
     * @return Returns the Credentials with the given email address.
     * @throws NoSuchEmailException Thrown if no user with the given email exists.
     */
    public Credentials getUserByEmail(String email) {
        Credentials credentials = credentialsRepository.findByEmail(email);
        if (credentials == null) {
            throw new NoSuchEmailException();
        } else {
            return credentials;
        }
    }

    /**
     * Gets the credentials by the given email and password.
     *
     * @param email The email that the queried user.
     * @param password The password of the queried user.
     * @return Returns the credentials object with the given email and password.
     * @throws InvalidPasswordException Thrown if the password doesn't match the user with the provided email.
     */
    public Credentials getUserByEmailAndPassword(String email, String password) {
        Credentials credentials = getUserByEmail(email);
        if (!bCryptPasswordEncoder.matches(password, credentials.getPassword())) {
            throw new InvalidPasswordException();
        } else {
            return credentials;
        }
    }

    /**
     * Saves a new user in the database.
     *
     * @param credentials The given Credentials that should be persisted.
     * @throws DuplicateEmailException Thrown if a user with the given email already exists in the database.
     */
    public void saveNewUser(Credentials credentials) {
        if (credentialsRepository.findByEmail(credentials.getEmail()) != null) {
            throw new DuplicateEmailException();
        }
        credentials.setPassword(bCryptPasswordEncoder.encode(credentials.getPassword()));
        credentials.setId(IDGenerator.getNextID());
        credentials.setRole(Role.ROLE_USER); // defaults to user
        credentialsRepository.save(credentials);
    }

    /**
     * Updates the email of the user with the given id to the new email.
     *
     * @param id The id of the user who wants to change their email.
     * @param newEmail The new email of the user.
     * @throws InvalidRequestException Thrown if there is no user with the given id.
     * @throws DuplicateEmailException Thrown if a user with the given email already exists in the database.
     */
    public void changeEmail(int id, String newEmail) {
        if (getUserById(id) == null) {
            throw new InvalidRequestException();
        }
        if (credentialsRepository.findByEmail(newEmail) != null) {
            throw new DuplicateEmailException();
        }
        Credentials credentialsToUpdate = credentialsRepository.getOne(id);
        credentialsToUpdate.setEmail(newEmail);
        credentialsRepository.save(credentialsToUpdate);
    }

    /**
     * Changes the password of the user with the given id to the new password.
     *
     * @param id The id of the user who wants to change their password.
     * @param oldPassword The old password of the user, used for authentication purposes.
     * @param newPassword The new password of the user.
     * @throws InvalidRequestException Thrown if there is no user with the given id.
     * @throws InvalidPasswordException Thrown if the provided old password doesn't match the actual old password.
     */
    public void changePassword(int id, String oldPassword, String newPassword) {
        if (getUserById(id) == null) {
            throw new InvalidRequestException();
        }
        Credentials credentialsToUpdate = credentialsRepository.getOne(id);
        if (!bCryptPasswordEncoder.matches(oldPassword, credentialsToUpdate.getPassword())) {
            throw new InvalidPasswordException();
        }
        credentialsToUpdate.setPassword(bCryptPasswordEncoder.encode(newPassword));
        credentialsRepository.save(credentialsToUpdate);
    }

    /**
     * Determines the error, if an error during
     * {@link JWTAuthenticationFilter#attemptAuthentication(HttpServletRequest,HttpServletResponse)} occurred.
     * 
     * This methods may seem like an anti-pattern, since Exceptions are usually thrown, not returned. 
     * In this particular case however, it is required, because the lack of a return type in the calling method doesn't
     * send the status code to the client. That's why the error has to be set in the response directly, which is
     * accessible across methods.
     *
     * @param credentials The faulty credentials.
     * @throws InvalidRequestException Thrown if the request json couldn't be mapped to a credentials object.
     * @throws NoSuchEmailException Thrown if the given email does not exist in the database.
     * @throws InvalidPasswordException Thrown if the password is not the password of the user with the given email.
     */
    public BoogleAuthException determineAuthenticationError(Credentials credentials) {
        if (credentials == null || credentials.getEmail() == null || credentials.getPassword() == null) {
            return new InvalidRequestException();
        } else if (credentialsRepository.findByEmail(credentials.getEmail()) == null) {
            return new NoSuchEmailException();
        } else if (!credentials.getPassword().
                matches((credentialsRepository.findByEmail(credentials.getEmail())).getPassword())) {
            return new InvalidPasswordException();
        }
        return null;
    }

    /**
     * Gets the currently highest id stored in the database and increments it by one.
     *
     * @return Return the next id that can be used to identify a new credentials object
     *         (not considering oauth_credentials).
     */
    public int getCurrentMaxID() {
        List<Credentials> credentialsList = credentialsRepository.findAll();
        int maxID = 0;
        for (Credentials creds : credentialsList) {
            if (creds.getId() > maxID) {
                maxID = creds.getId();
            }
        }
        return maxID;
    }

    /**
     * Gets the Credentials object based on the given id.
     *
     * @param id The id of the wanted credentials object.
     * @return Returns the object if the id exists in the database, null else.
     */
    public Credentials getUserById(int id) {
        if (credentialsRepository.findById(id).isPresent()) {
            return credentialsRepository.findById(id).get();
        } else {
            return null;
        }
    }

    /**
     * Deletes a user.
     * @param credentials The credentials of the user who gets deleted.
     */
    public void deleteUser(Credentials credentials) {
        Credentials toBeDeleted = getUserByEmailAndPassword(credentials.getEmail(), credentials.getPassword());
        credentialsRepository.deleteById(toBeDeleted.getId());
    }

    /**
     * Deletes the user with the given id.
     * This method is for admins only.
     *
     * @param id The id of the user who gets deleted.
     */
    public void deleteUserAdmin(int id) {
        credentialsRepository.deleteById(id);
    }

    /**
     * Called after the initialization of this service. It serves as test for docker volumes. If the test-
     * dataset is not persisted at startup, it will be added to the database. After shutting down the DB, you can verify
     * that the data is still there by accessing the database directly (using the commandline or any UI supporting
     * MariaDB). This is also the only user with role admin across the whole application, so do not delete this one!
     */
    @PostConstruct
    public void createAdmin() {
        if (credentialsRepository.count() == 0)  {
            Credentials admin = new Credentials();
            admin.setEmail("admin");
            admin.setPassword("password");
            saveNewUser(admin);
        }
    }
}
