package de.unipassau.ep.boogle.security.jwt;

import de.unipassau.ep.boogle.service.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static de.unipassau.ep.boogle.security.SecurityConstants.LOGIN_URL;
import static de.unipassau.ep.boogle.security.SecurityConstants.SIGN_UP_URL;


/**
 * The security config class tailored to the needs of our JWT authentication implementation.
 */
@Configuration
@EnableWebSecurity
public class JWTSecurity extends WebSecurityConfigurerAdapter {

    private final CredentialsService credentialsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Injects the dependencies.
     *
     * @param credentialsService The CredentialsService.
     * @param bCryptPasswordEncoder The PasswordEncoder.
     */
    @Autowired
    public JWTSecurity(CredentialsService credentialsService, BCryptPasswordEncoder bCryptPasswordEncoder
    ) {
        this.credentialsService = credentialsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Configures the security of the Boogle endpoints and adds the Authorization and Authentication filters.
     *
     * @param http The {@link HttpSecurity} object that is getting configured.
     * @throws Exception Throws an Exception in case of an error.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requestMatchers().and().cors().and().csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST, SIGN_UP_URL, LOGIN_URL).permitAll()
                .antMatchers(
                        "/**",
                        "https://github.com/**",
                        "https://api.github.com/**"
                ).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))

                // i don't know what this is doing, but i don't want to change it because it works
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    }

    /**
     * Configures the {@link AuthenticationManager} to use the {@link CredentialsService} as its
     * {@link org.springframework.security.core.userdetails.UserDetailsService} and the {@link BCryptPasswordEncoder} as
     * its {@link org.springframework.security.crypto.password.PasswordEncoder}.
     *
     * @param auth The {@link AuthenticationManagerBuilder} used to configure the {@link AuthenticationManager}.
     * @throws Exception Throws an Exception in case of an error.
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(credentialsService).passwordEncoder(bCryptPasswordEncoder);
    }
}
