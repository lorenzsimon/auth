package de.unipassau.ep.boogle.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import de.unipassau.ep.boogle.error.InvalidRequestException;
import de.unipassau.ep.boogle.model.entity.BlacklistedToken;
import de.unipassau.ep.boogle.model.entity.Credentials;
import de.unipassau.ep.boogle.repository.BlacklistRepository;
import de.unipassau.ep.boogle.service.CredentialsService;
import de.unipassau.ep.boogle.utility.JSONBuilder;
import de.unipassau.ep.boogle.utility.JSONUtility;
import de.unipassau.ep.logging.service.LoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Date;

import static de.unipassau.ep.boogle.security.SecurityConstants.*;

/**
 * The RestController for users who thankfully chose to put their trust into Boogle and decided to create their own
 * account.
 */
@RestController
public class CredentialsController {

    private final JSONBuilder jsonBuilder;
    private final CredentialsService credentialsService;
    private final WebClient.Builder webClientBuilder;
    private final LoggingService loggingService;
    private final BlacklistRepository blackListRepository;

    /**
     * Injects all dependencies.
     */
    @Autowired
    public CredentialsController(CredentialsService credentialsService, WebClient.Builder webClientBuilder,
                                 LoggingService loggingService, JSONBuilder jsonBuilder,
                                 BlacklistRepository blackListRepository) {
        this.jsonBuilder = jsonBuilder;
        this.credentialsService = credentialsService;
        this.webClientBuilder = webClientBuilder;
        this.loggingService = loggingService;
        this.blackListRepository = blackListRepository;
    }

    /**
     * Receives a json in the body of the request with the data necessary to create a {@link Credentials} object.
     * The new user gets stored in the database.
     * The response is the same as if the user entered their data to log in (id, token and refresh token), as a json.
     *
     * @param json The json containing the email and raw password.
     * @return Returns 201 Created if the user was successfully created, or 400 Bad Request with an error message,
     *         if any error occurred.
     */
    @PostMapping("/signup")
    public ResponseEntity<String> signUp(@RequestBody String json) {
        /*
        loggingService.add("apiEndpoint","/signup").send("api");
        loggingService
                .add("userID",credentials.getId())
                .add("action","signup")
                .send("userdata");
        */
        Credentials credentials = new Credentials();
        String email = JSONUtility.getValueFromKey(json, "email");
        String password = JSONUtility.getValueFromKey(json, "password");
        credentials.setEmail(email);
        credentials.setPassword(password);
        credentialsService.saveNewUser(credentials);

        String response = webClientBuilder
                .build()
                .post()
                .uri("http://service-auth:9000/login")
                .body(BodyInserters.fromValue(json))
                .header("Content-Type", "application/json")
                .retrieve()
                .bodyToMono(String.class)
                .block();
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /**
     * Receives a json in the body of the request with the data needed for authentication.
     * The user with these credentials gets removed from the database.
     *
     * @param json The json containing the email and raw password.
     * @return Returns 200 OK if the user was successfully deleted, or 400 BAD REQUEST if an error occurred.
     */
    @PostMapping("/delete")
    public ResponseEntity<String> delete(@RequestBody String json) {
        /*
        loggingService.add("apiEndpoint","/delete").send("api");
        loggingService
                .add("userID",credentials.getId())
                .add("action","delete")
                .send("userdata");
        */
        Credentials credentials = new Credentials();
        String email = JSONUtility.getValueFromKey(json, "email");
        String password = JSONUtility.getValueFromKey(json, "password");
        credentials.setEmail(email);
        credentials.setPassword(password);
        credentialsService.deleteUser(credentials);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Receives the id of the user who is about to get deleted. Deletes the user with the given id if it exists.
     * This mapping is for the admin only.
     *
     * @param json The json containing the id.
     * @return Returns 200 OK if the user was successfully deleted, or 400 BAD REQUEST if an error occurred.
     */
    @PostMapping("/delete_admin")
    public ResponseEntity<String> deleteAdmin(@RequestBody String json) {
        String id = JSONUtility.getValueFromKey(json ,"id");
        if (credentialsService.getUserById(Integer.parseInt(id)) != null) {
            credentialsService.deleteUserAdmin(Integer.parseInt(id));
        } else {
            throw new InvalidRequestException();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Receives a json with the id of the user and the new email. If the email is unique and the user with the given
     * id exists, the emails gets updated.
     *
     * @param json The json containing the id and new email.
     * @return Returns 200 OK if everything worked, or 400 BAD REQUEST if an error occurred.
     */
    @PostMapping("/change_email")
    public ResponseEntity<String> changeEmail(@RequestBody String json) {
        int id = Integer.parseInt(JSONUtility.getValueFromKey(json, "id"));
        String newEmail = JSONUtility.getValueFromKey(json, "newEmail");
        credentialsService.changeEmail(id, newEmail);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Receives a JSON with the id of the user and both the old and the new password. Changes the old password to
     * the new one, under the condition that the given old password and the actual old one match.
     *
     * @param json The json containing the id and both passwords.
     * @return Returns 200 OK if everything worked, 400 BAD REQUEST if an error occurred.
     */
    @PostMapping("/change_password")
    public ResponseEntity<String> changePassword(@RequestBody String json) {
        int id = Integer.parseInt(JSONUtility.getValueFromKey(json, "id"));
        String oldPassword = JSONUtility.getValueFromKey(json, "oldPassword");
        String newPassword = JSONUtility.getValueFromKey(json, "newPassword");
        credentialsService.changePassword(id, oldPassword, newPassword);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Gets the id from the payload of the token.
     *
     * @return Returns the id as a json. The status code is 200 OK if everything worked, and 400 Bad Request if not.
     */
    @GetMapping("/id")
    public ResponseEntity<String> getId(@RequestHeader("authorization") String token) {
        int id = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getClaim("id").asInt();
        String idAsJson = jsonBuilder
                .build()
                .setID(Integer.toString(id))
                .toJSON();

        return new ResponseEntity<>(idAsJson, HttpStatus.OK);
    }

    /**
     * Gets the email based on the id from the payload of token.
     *
     * @return Returns the email as a json. The status code is 200 OK if everything worked, and 400 Bad Request if not.
     */
    @GetMapping("/email")
    public ResponseEntity<String> getEmail(@RequestHeader("authorization") String token) {
        int id = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getClaim("id").asInt();
        String email = credentialsService.getUserById(id).getEmail();
        String emailAsJson = jsonBuilder
                .build()
                .setEmail(email)
                .toJSON();
        return new ResponseEntity<>(emailAsJson, HttpStatus.OK);
    }

    /**
     * Uses the refresh-token header to create a new access token for users.
     * If the given token is in fact a refresh token, the id from its payload is used to look up the user data from the
     * database. This data is used to generate a new access and refresh token.
     *
     * @param refreshToken The token used to generate the new tokens.
     * @return Returns 200 OK with a new access and refresh token if everything worked, and 400 Bad Request if not.
     */
    @PostMapping("/refresh")
    public ResponseEntity<String> refreshToken(@RequestHeader("refresh-token") String refreshToken) {
        boolean isRefreshToken = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                .build()
                .verify(refreshToken.replace(TOKEN_PREFIX, ""))
                .getClaim("isRefresh").asBoolean();

        if (isRefreshToken && blackListRepository.findBlacklistedTokenByToken(refreshToken) == null) {
            int id = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                    .build()
                    .verify(refreshToken.replace(TOKEN_PREFIX, ""))
                    .getClaim("id").asInt();

            Credentials usedForNewToken = credentialsService.getUserById(id);

            boolean isAdmin = (id == 0);

            String newAccessToken = JWT.create()
                    .withSubject((usedForNewToken.getEmail()))
                    .withClaim("isAdmin", isAdmin)
                    .withClaim("id", id)
                    .withClaim("isRefresh", false)
                    .withExpiresAt(new Date(System.currentTimeMillis() + TTL))
                    .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));

            String newRefreshToken = JWT.create()
                    .withClaim("id", id)
                    .withClaim("isRefresh", true)
                    .withClaim("isOAuth", false)
                    .withExpiresAt(new Date(System.currentTimeMillis() + REFRESH_TTL))
                    .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));


            String response = jsonBuilder
                    .build()
                    .setToken(newAccessToken)
                    .setAdditionalKeyValuePair("refresh_token", newRefreshToken)
                    .toJSON();

            BlacklistedToken blacklistedToken = new BlacklistedToken();
            blacklistedToken.setToken(refreshToken);
            blacklistedToken.setExpires(JWT.decode(refreshToken).getExpiresAt());
            blackListRepository.save(blacklistedToken);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        throw new InvalidRequestException();
    }
}