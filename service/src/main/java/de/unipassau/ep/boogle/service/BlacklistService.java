package de.unipassau.ep.boogle.service;

import de.unipassau.ep.boogle.model.entity.BlacklistedToken;
import de.unipassau.ep.boogle.repository.BlacklistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This service class deletes expired refresh tokens from the blacklist.
 */
@Service
@EnableScheduling
public class BlacklistService {

    private final BlacklistRepository blacklistRepository;

    /**
     * Injects the dependencies.
     * @param blacklistRepository The injected blackListRepository.
     */
    @Autowired
    public BlacklistService(BlacklistRepository blacklistRepository) {
        this.blacklistRepository = blacklistRepository;
    }

    /**
     * Deletes expired refresh tokens. Gets called every 10 minutes.
     */
    @Scheduled(fixedRate = 1000 * 60 * 10)
    private void whiteListTokens() {
        List<BlacklistedToken> tokens = blacklistRepository.findAll();
        List<BlacklistedToken> toBeDeletedTokens = new ArrayList<>();
        for (BlacklistedToken token : tokens) {
            if (token.getExpires().before(new Date())) {
                toBeDeletedTokens.add(token);
            }
        }
        for (BlacklistedToken deleteToken : toBeDeletedTokens) {
            blacklistRepository.delete(deleteToken);
        }
    }

}
