package de.unipassau.ep.boogle.init;

import de.unipassau.ep.logging.service.LoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ServiceInit implements CommandLineRunner {
    private final LoggingService logger;

    @Autowired
    public ServiceInit(LoggingService logger) {
        this.logger = logger;
    }

    @Override
    public void run(String... args) {
        initLogger();
    }

    private void initLogger() {
        logger.registerFields(new String[]{"action","userID"});
        logger.registerTags(new String[]{"credentialType","attempt"});
        logger.registerFields(new String[]{"apiEndpoint"});

    }
}