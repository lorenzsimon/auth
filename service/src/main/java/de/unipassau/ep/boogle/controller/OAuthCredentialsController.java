package de.unipassau.ep.boogle.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import de.unipassau.ep.boogle.error.InvalidRequestException;
import de.unipassau.ep.boogle.model.entity.BlacklistedToken;
import de.unipassau.ep.boogle.model.entity.OAuthCredentials;
import de.unipassau.ep.boogle.repository.BlacklistRepository;
import de.unipassau.ep.boogle.service.OAuthCredentialsService;
import de.unipassau.ep.boogle.utility.JSONBuilder;
import de.unipassau.ep.boogle.utility.JSONUtility;
import de.unipassau.ep.logging.service.LoggingService;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static de.unipassau.ep.boogle.security.SecurityConstants.*;
import static de.unipassau.ep.boogle.security.SecurityConstants.SECRET_KEY;

/**
 * The RestController for users who log in via Github.
 */
@RestController
public class OAuthCredentialsController {
    private static final String GITHUB_AUTHORIZE_URL = "https://github.com/login/oauth/authorize";
    private static final String GITHUB_TOKEN_URL = "https://github.com/login/oauth/access_token";
    private final static String GITHUB_USERDATA_URL = "https://api.github.com/user";

    private final Environment env;
    private final LoggingService loggingService;
    private final OAuthCredentialsService oAuthCredentialsService;
    private final WebClient.Builder webClientBuilder;
    private final JSONBuilder jsonBuilder;
    private final BlacklistRepository blackListRepository;


    // used to map the json to the id
    private final Map<String, String> idTokenMap = new HashMap<>();

    // used to map the csrf token to the id
    private final Map<String, String> idCSRFMap = new HashMap<>();

    /**
     * Injects the dependencies.
     *
     * @param webClientBuilder The WebClient for API requests.
     * @param loggingService The LoggingService.
     * @param oAuthCredentialsService The OAuthCredentialsService, handling operations on oauth_credentials.
     * @param env The environment, used to get the values of properties from this application.
     */
    @Autowired
    public OAuthCredentialsController(WebClient.Builder webClientBuilder,
                                      OAuthCredentialsService oAuthCredentialsService,
                                      Environment env, LoggingService loggingService, JSONBuilder jsonBuilder,
                                      BlacklistRepository blackListRepository) {
        this.webClientBuilder = webClientBuilder;
        this.loggingService = loggingService;
        this.oAuthCredentialsService = oAuthCredentialsService;
        this.env = env;
        this.jsonBuilder = jsonBuilder;
        this.blackListRepository = blackListRepository;
    }

    /**
     * Authenticates the user on Github by querying him for his credentials. None of the provided Github credentials are
     * visible for this service. After the user has entered the credentials, the method redirects to the
     * callback url (/login/oauth2/code/github) where the authorization is handled.
     *
     * For a more extensive documentation of the Github OAuth2 flow, please read into the
     * <a href="https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps">
     * Github documentation </a>.
     *
     * @param httpServletResponse The Object needed to seamlessly redirect the request to the authorization endpoint
     *                            after the user has been authorized.
     */
    @GetMapping("/oauthlogin")
    public void oauth(HttpServletResponse httpServletResponse) {
        //loggingService.add("apiEndpoint","/oauthlogin").send("api");
        String clientId = env.getProperty("spring.security.oauth2.client.registration.github.client-id");
        StringBuilder url = new StringBuilder(GITHUB_AUTHORIZE_URL);
        url.append("?client_id=");
        url.append(clientId);

        try {
            httpServletResponse.sendRedirect(url.toString());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Authorizes the user on github by checking the values provided in the url. After successful authorization, an
     * access_token is returned for github user data.
     *
     * @param code The parameter needed by Github in order to authorize a user.
     */
    @GetMapping("/login/oauth2/code/github")
    public void requestToken(@RequestParam String code, HttpServletResponse httpServletResponse) throws IOException {
        //loggingService.add("apiEndpoint","/login/oauth2/code/github").send("api");
        String clientId = env.getProperty("spring.security.oauth2.client.registration.github.client-id");
        String clientSecret = env.getProperty("spring.security.oauth2.client.registration.github.client-secret");

        String url = GITHUB_TOKEN_URL + "?client_id=" + clientId + "&client_secret=" + clientSecret + "&code=" + code;

        // gets the github access token by calling the url with the required query string
        String token = getGithubToken(url);

        // gets the id and nodeId from github with the token that now authorizes the user to access this data.
        OAuthCredentials credentials = getGithubUserData(token);


        StringBuilder requestBody = new StringBuilder("{\n    \"githubId\":\"");
        requestBody.append(credentials.getGithubId());
        requestBody.append("\",\n    \"nodeId\":\"");
        requestBody.append(credentials.getNodeId());
        requestBody.append("\"\n}");

        String tokenResponseForFrontend = webClientBuilder.build()
                .post()
                .uri("http://service-auth:9000/login2")
                .header("User-Agent", "Other")
                .body(Mono.just(requestBody), String.class)
                .retrieve()
                .bodyToMono(String.class)
                .block();

        String id = JSONUtility.getValueFromKey(tokenResponseForFrontend, "id");
        idTokenMap.put(id, tokenResponseForFrontend);
        idCSRFMap.put(id, RandomStringUtils.randomAlphanumeric(128));

        httpServletResponse.sendRedirect("http://localhost:8080/de/oauthlogin?id=" + id
                + "&token=" + idCSRFMap.get(id));
    }

    /**
     * Gets the response object including id, access and refresh token, based on the provided id and csrf token in the
     * body.
     *
     * @return Returns the response object.
     */
    @PostMapping("/token")
    public ResponseEntity<String> getResponseFromId(@RequestBody String json, @RequestHeader("csrf") String csrf) {
        String id = JSONUtility.getValueFromKey(json, "id");

        if (idTokenMap.get(id) == null || csrf == null || idCSRFMap.get(id) == null
                || !idCSRFMap.get((id)).equals(csrf)) {
            idCSRFMap.remove(id);
            throw new InvalidRequestException();
        }
        String response = idTokenMap.get(id);
        idTokenMap.remove(id);
        idCSRFMap.remove(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Sends the actual authorization request to Github.
     *
     * @param url The url containing the necessary parameters for user authorization at github.
     * @return Returns an access_token for the Github user data if no problems occurred, or null if the url contained
     *         invalid values.
     */
    private String getGithubToken(String url) {
        String json = webClientBuilder.build()
                .post()
                .uri(url)
                .header("Accept", "application/json")
                .retrieve()
                .bodyToMono(String.class)
                .block();

        if (json != null) {
            return JSONUtility.getValueFromKey(json, "access_token");
        }
        return null;
    }

    /**
     * Gets the githubId and nodeId of the user and creates a new OAuthCredentials object. The object is stored in the
     * database if doesn't exist yet.
     *
     * @param oauthToken The Token that authorizes the client to request the user data.
     * @return Returns the OAuthCredentials, so that the credentials can be sent to the /login2 endpoint.
     */
    private OAuthCredentials getGithubUserData(String oauthToken) {
        String userDataJson = webClientBuilder.build()
                .get()
                .uri(GITHUB_USERDATA_URL)
                .header("Authorization", "Bearer " + oauthToken)
                .retrieve()
                .bodyToMono(String.class)
                .block();

        OAuthCredentials oAuthCredentials = new OAuthCredentials();
        oAuthCredentials.setGithubId(JSONUtility.getValueFromKey(userDataJson, "id"));
        oAuthCredentials.setNodeId(JSONUtility.getValueFromKey(userDataJson, "node_id"));

        oAuthCredentialsService.saveNewUserIfNotExists(oAuthCredentials);

        return oAuthCredentials;
    }

    /**
     * Uses the refresh-token header to create a new access token for users.
     * If the given token is in fact a refresh token, the id from its payload is used to look up the user data from the
     * database. This data is used to generate a new access and refresh token.
     *
     * @param refreshToken The token used to generate the new tokens.
     * @return Returns 200 OK with a new access and refresh token if everything worked, and 400 Bad Request if not.
     */
    @PostMapping("/oauthrefresh")
    public ResponseEntity<String> refreshToken(@RequestHeader("refresh-token") String refreshToken) {
        boolean isRefreshToken = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                .build()
                .verify(refreshToken.replace(TOKEN_PREFIX, ""))
                .getClaim("isRefresh").asBoolean();

        if (isRefreshToken && blackListRepository.findBlacklistedTokenByToken(refreshToken) == null) {
            int id = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                    .build()
                    .verify(refreshToken.replace(TOKEN_PREFIX, ""))
                    .getClaim("id").asInt();

            OAuthCredentials usedForNewToken = oAuthCredentialsService.getUserById(id);
            String newAccessToken = JWT.create()
                    .withSubject((usedForNewToken.getGithubId()))
                    .withClaim("isAdmin", false)
                    .withClaim("id", id)
                    .withClaim("isRefresh", false)
                    .withExpiresAt(new Date(System.currentTimeMillis() + TTL))
                    .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));

            String newRefreshToken = JWT.create()
                    .withClaim("id", id)
                    .withClaim("isRefresh", true)
                    .withClaim("isOAuth", true)
                    .withExpiresAt(new Date(System.currentTimeMillis() + REFRESH_TTL))
                    .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));

            String response = jsonBuilder
                    .build()
                    .setToken(newAccessToken)
                    .setAdditionalKeyValuePair("refresh_token", newRefreshToken)
                    .toJSON();

            BlacklistedToken blacklistedToken = new BlacklistedToken();
            blacklistedToken.setToken(refreshToken);
            blacklistedToken.setExpires(JWT.decode(refreshToken).getExpiresAt());
            blackListRepository.save(blacklistedToken);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        throw new InvalidRequestException();
    }
}