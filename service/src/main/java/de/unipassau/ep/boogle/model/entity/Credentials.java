package de.unipassau.ep.boogle.model.entity;


import javax.persistence.*;
import java.io.Serializable;

/**
 * This entity class models all data necessary to uniquely identify and authenticate a user.
 */
@Entity
public class Credentials implements Serializable {

    @Id
    @Column(unique = true)
    private int id;

    private String email;

    private String password;

    private Role role;

    /**
     * The empty constructor for hibernate.
     */
    public Credentials() {

    }

    /**
     * Gets the email of a user.
     *
     * @return Returns the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email of a user.
     *
     * @param email The new email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the password of a user.
     *
     * @return Returns the password. Raw if not already saved in the DB, hashed, else.
     */
    public String getPassword() {
        return password;
    }

    /**
     * sets the password of a user.
     *
     * @param password The new password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the id of a user
     *
     * @return Returns the id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of a user. Don't use, because this is already handled by JPA.
     *
     * @param id The new id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the role of this user.
     *
     * @return Returns the role.
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets the role of a user/admin.
     *
     * @param role The role getting set.
     */
    public void setRole(Role role) {
        this.role = role;
    }
}
