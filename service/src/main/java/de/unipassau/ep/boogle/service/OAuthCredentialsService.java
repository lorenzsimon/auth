package de.unipassau.ep.boogle.service;

import de.unipassau.ep.boogle.component.IDGenerator;
import de.unipassau.ep.boogle.model.entity.OAuthCredentials;
import de.unipassau.ep.boogle.model.entity.Role;
import de.unipassau.ep.boogle.repository.OAuthCredentialsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This service class handles {@link OAuthCredentials}.
 * It serves as an interface between the controller and the database.
 */
@Service
public class OAuthCredentialsService implements UserDetailsService {

    private final OAuthCredentialsRepository oAuthCredentialsRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * The constructor injecting the {@link OAuthCredentialsRepository} and {@link BCryptPasswordEncoder} dependencies.
     *
     * @param oAuthCredentialsRepository The java connection to the db-auth.
     * @param bCryptPasswordEncoder The encoder used to encrypt passwords.
     */
    @Autowired
    public OAuthCredentialsService(OAuthCredentialsRepository oAuthCredentialsRepository,
                                   BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.oAuthCredentialsRepository = oAuthCredentialsRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Gets the currently highest id stored in the database and increments it by one.
     *
     * @return Return the next id that can be used to identify a new oauth_credentials object
     *         (not considering jwt credentials).
     */
    public int getCurrentMaxID() {
        List<OAuthCredentials> credentialsList = oAuthCredentialsRepository.findAll();
        int maxID = 0;
        for (OAuthCredentials creds : credentialsList) {
            if (creds.getId() > maxID) {
                maxID = creds.getId();
            }
        }
        return maxID;
    }

    /**
     * Gets a new {@link UserDetails} object of the {@link OAuthCredentials} object with the given githubId. This is
     * needed internally for spring security; that's also why it returns a UserDetails object instead of
     * OAuthCredentials.
     *
     * @param githubId The githubId of the requested UserDetails object.
     * @return Returns a UserDetails object with the given githubId if it exists.
     */
    @Override
    public UserDetails loadUserByUsername(String githubId) throws UsernameNotFoundException {
        OAuthCredentials credentials = oAuthCredentialsRepository.findByGithubId(githubId);
        return new User(credentials.getGithubId(), credentials.getNodeId(), new ArrayList<>());
    }

    /**
     * Gets the user by the given githubId.
     * @param githubId The githubId that the to be returned user has.
     * @return Returns the OAuthCredentials with the given githubId if it exists, null otherwise.
     */
    public OAuthCredentials getUserByGithubId(String githubId) {
        return oAuthCredentialsRepository.findByGithubId(githubId);
    }

    /**
     * Gets the Credentials object based on the given id.
     *
     * @param id The id of the wanted credentials object.
     * @return Returns the object if the id exists in the database, null else.
     */
    public OAuthCredentials getUserById(int id) {
        if (oAuthCredentialsRepository.findById(id).isPresent()) {
            return oAuthCredentialsRepository.findById(id).get();
        } else {
            return null;
        }
    }

    /**
     * Tries to save a new {@link OAuthCredentials} object in the database. If the githubId of the object is not unique,
     * an error is returned. If it is, the oauth_credentials get saved with the nodeId being hashed, and
     * is returned, which tells the user that the operation was successful.
     *
     * @param credentials The given OAuthCredentials that should be persisted.
     */
    public void saveNewUserIfNotExists(OAuthCredentials credentials) {
        if (oAuthCredentialsRepository.findByGithubId(credentials.getGithubId()) == null) {
            credentials.setNodeId(bCryptPasswordEncoder.encode(credentials.getNodeId()));
            credentials.setId(IDGenerator.getNextID());
            credentials.setRole(Role.ROLE_USER);
            oAuthCredentialsRepository.save(credentials);
        }
    }
}