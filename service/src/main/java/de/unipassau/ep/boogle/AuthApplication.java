package de.unipassau.ep.boogle;

import de.unipassau.ep.logging.service.LoggingService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * The entrypoint for the Auth-Service.
 *
 * For implementation reference, please visit
 * <a href="https://www.freecodecamp.org/news/how-to-setup-jwt-authorization-and-authentication-in-spring/">
 *     this free code camp tutorial</a>.
 *
 * Note: The terms "user" and "credentials" are used interchangeably in the documentation of this service and both refer
 *       to the {@link de.unipassau.ep.boogle.model.entity.Credentials} object.
 *       The term credentials can also refer to oauth_credentials (sometimes also oauthcredentials, higher- or
 *       lowercase). It should always be clear from the context of the method.
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AuthApplication {

	/**
	 * Starts the application.
	 * 
	 * @param args The args are unused.
	 */
	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}

	/**
	 * Defines a bean for the {@link BCryptPasswordEncoder}, so that it can be autowired.
	 *
	 * @return Returns the {@link BCryptPasswordEncoder}.
	 */
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Scope("singleton")
	public LoggingService loggingService() {
		return new LoggingService("auth");
	}

	/**
	 * Defines a bean for the {@link WebClient.Builder}, so that it can be autowired.
	 *
	 * @return Returns the {@link WebClient.Builder}.
	 */
	@Bean//(name="loadBalanced")
	//@LoadBalanced
	public WebClient.Builder webClient() {
		return WebClient.builder();
	}

/*
	@Bean(name="notLoadBalanced")
	public WebClient.Builder notLoadBalancedWebClient() {
		return WebClient.builder();
	}

 */
}
