package de.unipassau.ep.boogle.repository;

import de.unipassau.ep.boogle.model.entity.BlacklistedToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This repository connects the service to the authentication database (db-auth).
 * The class in particular handles refresh tokens that have been used once. They are stored for as long as they are
 * valid, which means 24 hours. Tokens in here are not usable anymore.
 */
@Repository
public interface BlacklistRepository extends JpaRepository<BlacklistedToken, Integer> {

    /**
     * Queries the database for the given token.
     *
     * @param token The given token.
     * @return Returns a BlackListedToken object if the given token string exists, or null else.
     */
    BlacklistedToken findBlacklistedTokenByToken(String token);

}
